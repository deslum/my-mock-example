package repository

import (
	"my-mock-example/postgres/model"
)

type IRepository interface {
	GetContents() []*model.Content
	SetContents(contents []*model.Content)
}
