package repository

import (
	"my-mock-example/postgres/model"
)

type PostgresRepository struct {
}

func NewPostgresRepository(address string) *PostgresRepository {
	return &PostgresRepository{}
}

func (o *PostgresRepository) GetContents() []*model.Content {
	return nil
}
