package repository

import (
	"my-mock-example/postgres/model"
)

type PostgresMock struct {
	contents []*model.Content
}

func NewPostgresMock() *PostgresMock {
	return &PostgresMock{
		contents: make([]*model.Content, 0),
	}
}

func (o *PostgresMock) GetContents() []*model.Content {
	return o.contents
}

func (o *PostgresMock) SetContents(contents []*model.Content) {
	o.contents = contents
}
