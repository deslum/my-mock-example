package example

import (
	"my-mock-example/postgres/model"
	"my-mock-example/postgres/repository"
	"reflect"
	"testing"
)

var contents = []*model.Content{
	{
		Id:       1,
		Name:     "Test1",
		IsActive: false,
	},
	{
		Id:       2,
		Name:     "Test2",
		IsActive: false,
	},
	{
		Id:       3,
		Name:     "Test3",
		IsActive: true,
	},
}

var tests = []struct {
	name     string
	repo     repository.IRepository
	contents []*model.Content
	want     []*model.Content
}{
	{
		name:     "Case1. Get all active contents",
		repo:     repository.NewPostgresMock(),
		contents: contents,
		want: []*model.Content{
			{
				Id:       3,
				Name:     "Test3",
				IsActive: true,
			},
		},
	},
}

func TestExample_GetActiveContents(t *testing.T) {

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ex := NewExample(tt.repo)
			ex.repo.SetContents(tt.contents)
			if got := ex.GetActiveContents(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetActiveContents() = %v, want %v", got, tt.want)
			}
		})
	}
}
