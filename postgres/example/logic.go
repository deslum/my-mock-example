package example

import (
	"my-mock-example/postgres/model"
	"my-mock-example/postgres/repository"
)

type Example struct {
	repo repository.IRepository
}

func NewExample(repo repository.IRepository) *Example {
	return &Example{
		repo: repo,
	}
}

func (o *Example) GetActiveContents() []*model.Content {
	result := make([]*model.Content, 0)
	contents := o.repo.GetContents()
	for _, content := range contents {
		if content.IsActive {
			result = append(result, content)
		}
	}

	return result
}
