package model

type Content struct {
	Id       int64
	Name     string
	IsActive bool
}
