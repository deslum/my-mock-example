package repository

type Mock struct {
	db map[string]string
}

func NewMock() *Mock {
	return &Mock{
		db: make(map[string]string, 0),
	}
}

func (o *Mock) HGet(key string) string {
	return o.db[key]
}

func (o *Mock) HSet(key string, value string) error {
	o.db[key] = value
	return nil
}
