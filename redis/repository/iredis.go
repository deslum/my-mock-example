package repository

type IRedis interface {
	HGet(key string) string
	HSet(key string, value string) error
}
