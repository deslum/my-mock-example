package example

import (
	"my-mock-example/redis/repository"
	"strconv"
)

type KV struct {
	redis repository.IRedis
}

func NewKV(redis repository.IRedis) *KV {
	return &KV{
		redis: redis,
	}
}

func (o *KV) GetAll() map[string]string {
	var res = make(map[string]string, 0)
	for i := 0; i <= 5; i++ {
		value := o.redis.HGet(strconv.Itoa(i))
		res[strconv.Itoa(i)] = value
	}

	return res
}
