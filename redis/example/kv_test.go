package example

import (
	"my-mock-example/redis/repository"
	"reflect"
	"testing"
)

func TestKV_GetAll(t *testing.T) {
	tests := []struct {
		name      string
		redisMock repository.IRedis
		data      map[string]string
		want      map[string]string
	}{
		{
			name:      "Test 1. Bla bla bla",
			redisMock: repository.NewMock(),
			data: map[string]string{
				"0": "0",
				"1": "1",
				"2": "2",
				"3": "3",
				"4": "4",
				"5": "5",
			},
			want: map[string]string{
				"0": "0",
				"1": "1",
				"2": "2",
				"3": "3",
				"4": "4",
				"5": "5",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for k, v := range tt.data {
				tt.redisMock.HSet(k, v)
			}

			o := NewKV(tt.redisMock)
			if got := o.GetAll(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetAll() = %v, want %v", got, tt.want)
			}
		})
	}
}
